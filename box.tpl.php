<div class="box">

<?php if ($title): ?>
  <h3><?php echo $title ?></h3>
<?php endif; ?>

  <div class="content"><?php echo $content ?></div>
</div>

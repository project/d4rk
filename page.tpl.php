<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language ?>" lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>">

<head>
  <title><?php echo $head_title; ?></title>
  <?php echo $head; ?>
  <?php echo $styles; ?>
  <!--[if IE 7]>
  <style type="text/css" media="all">
    @import "<?php echo $base_path . path_to_theme() ?>/css/ie7.css";
  </style>
  <![endif]-->
  <?php echo $scripts; ?>
</head>

<body class="<?php echo $body_classes; ?>">
  <div id="skip-nav"><a href="#content">Skip to Content</a></div>  
  <div id="page">
	
	<!-- ______________________ HEADER _______________________ -->
  
	<div id="header">

		<?php if ($header): ?>
            <div id="header-region" class="inner">
                <?php echo $header; ?>
            </div>
        <?php endif; ?>
        
        <div id="navi-search-secondary">
        
        <?php if($search_box || $secondary_links): ?>
        
            <div id="search-secondary">
            
				<?php if($search_box): ?>
                    <div id="search" class="container-inline"><?php echo $search_box; ?></div>
                <?php endif; ?>
                
                <?php if (!empty($secondary_links)): ?>
                  <div id="secondary" class="clear-block">
                    <?php echo theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
                </div><?php endif; ?>
                
            </div>
        
        <?php endif; ?>
		
        <div id="navigation" class="menu <?php if (!empty($primary_links)) { echo "withprimary"; } if (!empty($secondary_links)) { echo " withsecondary"; } ?> ">
            <?php if (!empty($primary_links)): ?>
              <div id="primary" class="clear-block">
                <?php echo theme('links', $primary_links, array('class' => 'links primary-links')); ?>
        </div><?php endif; ?>

      </div> <!-- /navigation -->
      </div>
	
    </div> <!-- /header -->
        
        <div id="banner" class="codeclear">
            
            <?php if ($banner): ?>
				<div id="banner-region">
					<?php echo $banner; ?>
				</div>
			<?php endif; ?>
            
            <div id="logo-title"><center>
        
            <?php if (!empty($logo)): ?>
              <a href="<?php echo $front_page; ?>" title="<?php echo t('Home'); ?>" rel="home" id="logo">
                <img src="<?php echo $logo; ?>" alt="<?php echo t('Home'); ?>" />
              </a>
            <?php endif; ?>
            
            <div id="name-and-slogan">
              <?php if (!empty($site_name)): ?>
                <h1 id="site-name">
                  <a href="<?php echo $front_page ?>" title="<?php echo t('Home'); ?>" rel="home"><span><?php echo $site_name; ?></span></a>
                </h1>
              <?php endif; ?>
    
              <?php if (!empty($site_slogan)): ?>
                <div id="site-slogan"><?php echo $site_slogan; ?></div>
              <?php endif; ?>
            </div></center> <!-- /name-and-slogan -->
            
            </div> <!-- /logo-title -->
        
        </div> <!-- /banner -->

			<!-- ______________________ MAIN _______________________ -->
  	
    	<div id="main" class="codeclear">
		
	  			<div id="content" class="column">
						<div id="content-inner">
              
  	  		  	<?php if ($content_top): ?>
								<div id="content-top" class="inner">
									<?php echo $content_top; ?>
								</div>
							<?php endif; ?>

		        	<?php if ($tabs || $messages || $mission): ?>
		        	  <div id="content-header">
                      
                      <?php if ($mission): ?>
										<div id="mission" class="inner"><?php echo $mission; ?></div>
					  <?php endif; ?>
                      
                      <?php if ($messages): ?>
										<div id="messages" class="inner"><?php echo $messages; ?></div>
					  <?php endif; ?>
                      
                      <?php if ($tabs): ?>
										<div id="tabs"><?php echo $tabs; ?></div>
					  <?php endif; ?>
		
		        	  </div> <!-- /#content-header -->
		        	<?php endif; ?>
            	
		        <div id="content-area" class="inner"> <!-- CONTENT AREA -->
                <?php if ($breadcrumb || $title): ?>
                	<div id="content-header-bar">
                   	<?php echo $breadcrumb; ?>
		        	<?php if ($title): ?>
		        	      <h1 class="title"><?php echo $title; ?></h1>
		        	<?php endif; ?></div><?php endif; ?>
		
		        	<?php echo $help; ?> 
		        	<?php echo $content; ?> 
                    <?php echo $feed_icons; ?>
		       	</div>

  	  		  	<?php if ($content_bottom): ?>
								<div id="content-bottom" class="inner">
									<?php echo $content_bottom; ?>
								</div>
				<?php endif; ?>
	
  	  			</div>
		</div> <!-- /content-inner /content -->

		<div id="sidebar" class="column">
            
            <?php if ($sidebar_top): ?> <!-- SIDEBAR TOP -->
				<div id="sidebar-top" class="inner">
					<?php echo $sidebar_top; ?>
				</div>
			<?php endif; ?>
  	  		
			<?php if ($left): ?> <!-- SIDEBAR LEFT -->
  	  		  <div id="sidebar-left">
							<div id="sidebar-left-inner">
							  <?php echo $left; ?>
							</div>
  	  		  </div> <!-- /sidebar-left -->
  	  		<?php endif; ?>
      		
  	  		<?php if ($right): ?> <!-- SIDEBAR RIGHT -->
  	  		  <div id="sidebar-right">
							<div id="sidebar-right-inner">
								<?php echo $right; ?>
							</div>
  	  		  </div> <!-- /sidebar-right -->
  	  		<?php endif; ?>
            
            <?php if ($sidebar_bottom): ?> <!-- SIDEBAR BOTTOM -->
				<div id="sidebar-bottom" class="inner">
					<?php echo $sidebar_bottom; ?>
				</div>
			<?php endif; ?>     
            
            </div>
            
            <div id="contactcard">
            	<center>
                <div id="name-and-slogan">
                  <?php if (!empty($site_name)): ?>
                    <h1 id="site-name">
                      <a href="<?php echo $front_page ?>" title="<?php echo t('Home'); ?>" rel="home"><span><?php echo $site_name; ?></span></a>
                    </h1>
                  <?php endif; ?>
        
                  <?php if (!empty($site_slogan)): ?>
                    <div id="site-slogan"><?php echo $site_slogan; ?></div>
                  <?php endif; ?>
                  
                  <?php if(!empty($footer_message)): ?>
                  	<div id="footer-message"><?php echo $footer_message; ?></div>
                   <?php endif; ?>
                </div></center> <!-- /name-and-slogan -->
            
            </div>     
	  
  	</div> <!-- /main -->
  	
		<!-- ______________________ FOOTER _______________________ -->

  	  <div id="footer">
	      <?php echo $footer_block; ?>
          <div class="themecredits">Theme provided by <a href="http://www.textcomedia.com">TextCo Media</a>. under GPL license from Webdobe <a href="http://www.webdobe.com">Drupal themes</a></div>
  	  </div> <!-- /footer -->
		
  	<?php echo $closure; ?>
  </div> <!-- /page -->

</body>
</html>